<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit999b200fc8cdd382e8a8c3601bbbf00c
{
    public static $files = array (
        '253c157292f75eb38082b5acb06f3f01' => __DIR__ . '/..' . '/nikic/fast-route/src/functions.php',
    );

    public static $prefixLengthsPsr4 = array (
        'i' => 
        array (
            'itprojects\\' => 11,
        ),
        'F' => 
        array (
            'FastRoute\\' => 10,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'itprojects\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
        'FastRoute\\' => 
        array (
            0 => __DIR__ . '/..' . '/nikic/fast-route/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit999b200fc8cdd382e8a8c3601bbbf00c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit999b200fc8cdd382e8a8c3601bbbf00c::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
