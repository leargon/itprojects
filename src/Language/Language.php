<?php


namespace itprojects\Language;


class Language
{
    public static  $ru = [
        'login_button' => 'Войти',
        'reg_button' => 'Регистрация',
        'admin_button' => 'Выход',
        'login_h1' => 'Добро пожаловать',
        'reg_h1' => 'Форма регистрации нового пользователя',
        'admin_h1' => 'Добро пожаловать в административную часть',
        'to_register' => 'Если у вас еще нет аккаунта  - ',
        'to_login' => 'Если у вас уже есть аккаунт  - ',
        'login_email_placeholder' => 'Ваш email',
        'login_pass_placeholder' => 'Пароль',
        'reg_email_placeholder' => 'Ваш email',
        'reg_pass_placeholder' => 'Пароль',
        'reg_name_placeholder' => 'Ваш логин',
        'admin_greeting_msg' => 'Здравствуйте',
        'admin_mail_msg' => 'Ваш почтовый ящик:',
    ];
    public static  $en = [
        'login_button' => 'Login',
        'reg_button' => 'Register',
        'admin_button' => 'Logout',
        'login_h1' => 'You are welcome!',
        'reg_h1' => 'Register form of new user',
        'admin_h1' => 'Welcome to dashboard',
        'to_register' => "If you don't have an account  - ",
        'to_login' => 'If you already have an account  - ',
        'login_email_placeholder' => 'Email',
        'login_pass_placeholder' => 'Password',
        'reg_email_placeholder' => 'Email',
        'reg_pass_placeholder' => 'Password',
        'reg_name_placeholder' => 'Login',
        'admin_greeting_msg' => 'Hello',
        'admin_mail_msg' => 'Your email:',
    ];

}