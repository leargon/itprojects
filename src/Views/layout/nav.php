<?php if (isset($_SESSION['user'])): ?>

            <form action="/logout" method="post">
                <button class="btn btn-info btn-lg btn-block" name="submit" type="submit"><?=$dict['admin_button']?></button>
            </form>

<?php endif; ?>
