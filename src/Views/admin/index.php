<?php require_once dirname(__DIR__) . '/layout/header.php'; ?>

<div class="container-fluid h-100">
    <div class="row justify-content-center align-items-center h-100">
        <div class="col col-sm-6 col-md-6 col-lg-4 col-xl-3 shadow p-3 mb-5 rounded bg-light">
            <h3 class="mb-4"><?=$dict['admin_h1']?></h3>

            <?php if (file_exists('./img/' . $user['name'] . '.jpg')): ?>
            <img src="/img/<?=$user['name']?>.jpg" class="img-fluid rounded mx-auto d-block">
            <?php endif; ?>

            <p><?=$dict['admin_greeting_msg']?>, <span class="font-weight-bold"><?=$user['name']?></span></p>
            <p><?=$dict['admin_mail_msg']?> <span class="font-weight-bold"><?=$user['email']?></span></p>
            <?php require_once dirname(__DIR__) . '/layout/nav.php'; ?>
            <br>
            <?php require_once dirname(__DIR__) . '/layout/lang.php'; ?>
        </div>
    </div>
</div>

<?php require_once dirname(__DIR__) . '/layout/footer.php'; ?>
