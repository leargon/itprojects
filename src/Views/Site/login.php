<?php require_once dirname(__DIR__) . '/layout/header.php'; ?>

<div class="container-fluid h-100">
    <div class="row justify-content-center align-items-center h-100">
        <div class="col col-sm-6 col-md-6 col-lg-4 col-xl-3">
            <h3><?=$dict['login_h1']?></h3>
            <?php if(isset($errors) && is_array($errors)): ?>
                <div class="alert alert-danger" role="alert">
                    <?php foreach ($errors as $error): ?>
                        <?='- '.$error.'<br>'?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <form action="" method="post">
                <div class="form-group">
                    <input class="form-control form-control-lg" placeholder="<?=$dict['login_email_placeholder']?>" type="text" name="email">
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" placeholder="<?=$dict['login_pass_placeholder']?>" type="password" name="password">
                </div>
                <div class="form-group">
                    <button class="btn btn-info btn-lg btn-block" name="submit"><?=$dict['login_button']?></button>
                </div>
            </form>
            <p><?=$dict['to_register']?><a class="text-decoration-none" href="/register"><?=$dict['reg_button']?></a></p>
            <?php require_once dirname(__DIR__) . '/layout/lang.php'; ?>
        </div>
    </div>
</div>

<?php require_once dirname(__DIR__) . '/layout/footer.php'; ?>
