<?php require_once dirname(__DIR__) . '/layout/header.php'; ?>

<div class="container-fluid h-100">
    <div class="row justify-content-center align-items-center h-100">
        <div class="col col-sm-6 col-md-6 col-lg-4 col-xl-3">
            <h3 class="mb-4"><?=$dict['login_h1']?></h3>
            <?php if (isset($success)): ?>
                <div class="alert alert-success" role="alert">
                    <?=$success?>
                </div>
            <?php endif; ?>
            <?php if(isset($errors) && is_array($errors)): ?>
                <div class="alert alert-danger" role="alert">
                    <?php foreach ($errors as $error): ?>
                        <?='- '.$error.'<br>'?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <input class="form-control form-control-lg" placeholder="<?=$dict['reg_name_placeholder']?>" type="text" name="name" pattern="[a-zA-Z]+" minlength="6" maxlength="16" required>
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" placeholder="<?=$dict['reg_email_placeholder']?>" type="text" name="email" required>
                </div>
                <div class="form-group">
                    <input class="form-control form-control-lg" placeholder="<?=$dict['reg_pass_placeholder']?>" type="password" name="password" minlength="8" maxlength="16" required>
                </div>
                <div class="form-group">
<!--                    <label for="exampleFormControlFile1">Example file input</label>-->
                    <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                </div>
                <div class="form-group">
                    <button class="btn btn-info btn-lg btn-block" name="submit" type="submit"><?=$dict['reg_button']?></button>
                </div>
            </form>
            <p><?=$dict['to_login']?><a class="text-decoration-none" href="/"><?=$dict['login_button']?></a></p>
            <?php require_once dirname(__DIR__) . '/layout/lang.php'; ?>
        </div>
    </div>
</div>
<?php require_once dirname(__DIR__) . '/layout/footer.php'; ?>
