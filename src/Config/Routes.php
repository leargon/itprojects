<?php

return [
	 ['GET', '/', '\AccountController/actionLogin'],
     ['POST', '/', '\AccountController/actionLogin'],
     ['GET', '/register', '\AccountController/actionRegister'],
     ['POST', '/register', '\AccountController/actionRegister'],
	 ['GET', '/admin', '\SiteController/admin'],
     ['POST', '/logout', '\AccountController/actionLogout'],
     ['POST', '/ru', '\LanguageController/setLang'],
     ['POST', '/en', '\LanguageController/setLang'],
];