<?php


namespace itprojects\Models;
use itprojects\Components\Db;
use \PDO;

class Account
{
    // Добавление аккаунта в бд
    public static function addAccount($name, $email, $password)
    {
        $db = Db::getConnection();
        $password = password_hash($password, PASSWORD_DEFAULT);
        $sql = 'INSERT INTO user (name, email, password) '
            . 'VALUES (:name, :email, :password)';
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        return $result->execute();
    }

    public static function checkUserData($email, $password)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM user WHERE email = :email';
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        //$result->bindParam(':password', $password, PDO::PARAM_INT);
        $result->execute();
        $user = $result->fetch();

        if ($user && password_verify($password, $user['password'])) {
            return $user['id'];
        }
        return false;
    }

    public static function checkName($name)
    {
        $valid = TRUE;
        $len = mb_strlen($name);
        if (($len < 6) || ($len > 16))
        {
            $valid = FALSE;
        }
        if (!filter_var($name, FILTER_SANITIZE_STRING)) {
            $valid = FALSE;
        }
        return $valid;
    }

    public static function checkEmail($email)
    {
        $valid = true;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return $valid;
    }

    public static function checkPassword($password)
    {
        $valid = true;
        if (strlen($password) < 8) {
            $valid = false;
        }
        return $valid;
    }

    public static function auth($userId)
    {
        $_SESSION['user'] = $userId;
    }

    public static function getUserById($id)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'SELECT * FROM user WHERE id = :id';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        // Указываем, что хотим получить данные в виде массива
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        return $result->fetch();
    }

    public static function checkLogged()
    {
        // Если сессия есть, вернем идентификатор пользователя
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }

        header("Location: /");
    }

    public static function checkEmailExists($email)
    {
        // Соединение с БД
        $db = Db::getConnection();

        // Текст запроса к БД
        $sql = 'SELECT COUNT(*) FROM user WHERE email = :email';

        // Получение результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();

        if ($result->fetchColumn())
            return true;
        return false;
    }
}