<?php

namespace itprojects\Controllers;

use itprojects\Language\Language;
use itprojects\Models\Account;
use itprojects\Models\Tasks;
/**
 * 
 */
class SiteController
{
    public function admin()
    {
        $userId = Account::checkLogged();
        $user = Account::getUserById($userId);
        $dict = Language::$ru;
        if (@$_SESSION['lang'] == 'en')
        {
            $dict = Language::$en;
        }
        if (!$user)
        {
            die('Access denied');
        }

        require_once dirname(__DIR__) . '/Views/admin/index.php';
    }

}