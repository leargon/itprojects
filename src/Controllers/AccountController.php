<?php


namespace itprojects\Controllers;
use itprojects\Models\Account;
use itprojects\Language\Language;

class AccountController
{
    public function actionLogin()
    {
        $password = false;
        $email = false;
        $dict = Language::$ru;
        if (@$_SESSION['lang'] == 'en')
        {
            $dict = Language::$en;
        }

        if (isset($_POST['submit']))
        {
            $password = $_POST['password'];
            $email = $_POST['email'];

            $errors = false;

            if (!Account::checkEmail($email))
            {
                $errors[] = 'Неверный емаил';
            }

            if (!Account::checkPassword($password))
            {
                $errors[] = 'Неверный пароль';
            }

            $userId = Account::checkUserData($email, $password);

            if ($userId == false)
            {
                $errors[] = 'Неправильные данные для входа на сайт';
            }

            if ($userId == true)
            {
                Account::auth($userId);
                header('location: /admin');
            }
        }

        require_once dirname(__DIR__) . '/Views/Site/login.php';
    }

    public function actionRegister()
    {
        $name = false;
        $password = false;
        $email = false;

        $dict = Language::$ru;
        if (@$_SESSION['lang'] == 'en')
        {
            $dict = Language::$en;
        }

        if (isset($_POST['submit']))
        {
            $name = $_POST['name'];
            $password = $_POST['password'];
            $email = $_POST['email'];

            $errors = false;

            if (!Account::checkName($name))
            {
                $errors[] = 'Логин должен быть не меньше 6 символов';
            }

            if (!Account::checkPassword($password))
            {
                $errors[] = 'Пароль должен быть длиной не меньше 8 символов';
            }

            if (!Account::checkEmail($email))
            {
                $errors[] = 'Неверный формат email';
            }

            if (Account::checkEmailExists($email)) {
                $errors[] = 'Такой email уже используется';
            }

            if ($errors == false)
            {
                if(Account::addAccount($name, $email, $password))
                {
                    if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
                        // Если загружалось, переместим его в нужную папке, дадим новое имя
                        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/img/{$name}.jpg");
                    }
                    $success = "Вы успешно зарегистрировались, можете войти с вашими учетными данными";
                }
            }
        }
        require_once dirname(__DIR__) . '/Views/Site/register.php';
    }

    public function actionLogout()
    {
        session_start();

        // Удаляем информацию о пользователе из сессии
        unset($_SESSION["user"]);

        // Перенаправляем пользователя на главную страницу
        header("Location: /");
    }
}