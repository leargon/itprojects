<?php

namespace itprojects\Components;

use FastRoute;
/**
 * 
 */
class Router 
{
	
	public function run()
	{
		$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {
		    $namespace = '\itprojects\Controllers';
		    $routes = include_once(dirname(__DIR__) . '/Config/Routes.php');
		    foreach ($routes as $key => $route) {
		    	$r->addRoute($route[0], $route[1], $namespace . $route[2]);
		    }
		});

		// Fetch method and URI from somewhere
		$httpMethod = $_SERVER['REQUEST_METHOD'];
		$uri = $_SERVER['REQUEST_URI'];

		// Strip query string (?foo=bar) and decode URI
		if (false !== $pos = strpos($uri, '?')) {
		    $uri = substr($uri, 0, $pos);
		}
		$uri = rawurldecode($uri);

		$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
		switch ($routeInfo[0]) {
		    case FastRoute\Dispatcher::NOT_FOUND:
		        echo "Ничего не найдено";
		        break;
		    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
		        $allowedMethods = $routeInfo[1];
		        echo "Запрещено";
		        break;
		    case FastRoute\Dispatcher::FOUND:
		        $handler = $routeInfo[1];
		        $vars = $routeInfo[2];
		        list($class, $method) = explode("/", $handler, 2);
		        call_user_func_array([new $class, $method], $vars);
		        break;
		}
	}
}